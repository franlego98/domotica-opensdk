
$(document).ready(function() {
	var retries;
	var ws;
	var ws2;
	
	function wsOpen(){
		ws = new WebSocket("ws://" + location.host);
		ws.binaryType = 'arraybuffer';
		ws.onopen = function(evt) { retries = 0; console.log("done", "WebSocket is open."); };
		ws.onerror = ;
		ws.onmessage = ;
		ws.onclose = ;
		
		ws2 = new WebSocket("ws://" + location.host + "/stream");
		ws2.onopen = function(evt) { $("#msg").text("Conectado!"); };
		ws2.onmessage = function(e) { $("#msg").text("Conectado!"); update_buttons(JSON.parse(e.data)); }
		ws2.onerror = 
		ws2.onclose = ;
	}
	
	wsOpen();
	
	function wsWrite(data) {
		if (ws.readyState == 3)
			wsOpen();
		
		if (ws.readyState == 1)
			ws2.send(data);
	}
	
	$("#jardin-on").text("Encender");
	$("#jardin-on").click( function(e) {
		wsWrite("A");
	});

	$("#jardin-off").text("Apagar");
	$("#jardin-off").click( function(e) {
		wsWrite("B");
	});
	
	$("#sotano-on").text("Encender");
	$("#sotano-on").click( function(e) {
		wsWrite("C");
	})

	$("#sotano-off").text("Apagar");
	$("#sotano-off").click( function(e) {
		wsWrite("D");
	});
	
	function update_buttons(data){
		switch (data["jardin"]) {
			case 0:
				$("#jardin-on").removeClass("outline");
				$("#jardin-on").removeAttr("disabled");
				$("#jardin-off").addClass("outline");
				$("#jardin-off").attr("disabled","");
				break;
			case 1:
				$("#jardin-off").removeClass("outline");
				$("#jardin-off").removeAttr("disabled");
				$("#jardin-on").addClass("outline");
				$("#jardin-on").attr("disabled","");
				break;
		}
		
		switch (data["sotano"]) {
			case 0:
				$("#sotano-on").removeClass("outline");
				$("#sotano-on").removeAttr("disabled");
				$("#sotano-off").addClass("outline");
				$("#sotano-off").attr("disabled","");
				break;
			case 1:
				$("#sotano-off").removeClass("outline");
				$("#sotano-off").removeAttr("disabled");
				$("#sotano-on").addClass("outline");
				$("#sotano-on").attr("disabled","");
				break;
		}
	}
});
