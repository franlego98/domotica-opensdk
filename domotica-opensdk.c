#include <espressif/esp_common.h>
#include <esp8266.h>
#include <esp/uart.h>
#include <string.h>
#include <stdio.h>
#include <FreeRTOS.h>
#include <task.h>
#include <ssid_config.h>
#include <httpd/httpd.h>

int jardin,sotano;

#define gpio_jardin 4
#define gpio_sotano 5

enum {
    JARDIN_ON,
    JARDIN_OFF,
    SOTANO_ON,
    SOTANO_OFF
};

int32_t ssi_handler(int32_t iIndex, char *pcInsert, int32_t iInsertLen)
{
    switch (iIndex) {
        case JARDIN_ON:
			snprintf(pcInsert, iInsertLen, jardin == 1 ? " outline " : " ");
			break;
		case JARDIN_OFF:
			snprintf(pcInsert, iInsertLen, jardin == 0 ? " outline " : " ");
			break;
		case SOTANO_ON:
			snprintf(pcInsert, iInsertLen, sotano == 1 ? " outline " : " ");
			break;
		case SOTANO_OFF:
			snprintf(pcInsert, iInsertLen, sotano == 0 ? " outline " : " ");
			break;
        default:
            snprintf(pcInsert, iInsertLen, "N/A");
            break;
    }

    /* Tell the server how many characters to insert */
    return (strlen(pcInsert));
}

const char *api_cgi_handler(int iIndex, int iNumParams, char *pcParam[], char *pcValue[])
{
	for (int i = 0; i < iNumParams; i++) {
        if (strcmp(pcParam[i], "jardin") == 0) {
            jardin = atoi(pcValue[i]);
		} else if (strcmp(pcParam[i], "sotano") == 0) {
            sotano = atoi(pcValue[i]);
		}
	}
	
	actualizar_luces();
	return "/index.ssi";
}

void websocket_task(void *pvParameter)
{
    struct tcp_pcb *pcb = (struct tcp_pcb *) pvParameter;

    for (;;) {
        if (pcb == NULL || pcb->state != ESTABLISHED) {
            printf("Connection closed, deleting task\n");
            break;
        }

        /* Generate response in JSON format */
        char response[64];
        int len = snprintf(response, sizeof (response),
                "{\"jardin\" : %d,"
                " \"sotano\" : %d}", jardin,sotano);
        if (len < sizeof (response))
            websocket_write(pcb, (unsigned char *) response, len, WS_TEXT_MODE);

        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }

    vTaskDelete(NULL);
}

void websocket_cb(struct tcp_pcb *pcb, uint8_t *data, u16_t data_len, uint8_t mode)
{
    printf("[websocket_callback]:\n%.*s\n", (int) data_len, (char*) data);

    switch (data[0]) {
        case 'A': // ADC
            jardin=1;
            break;
        case 'B': // Disable LED
            jardin=0;
            break;
        case 'C': // Enable LED
            sotano=1;
            break;
        case 'D': // Enable LED
            sotano=0;
            break;
        default:
            printf("Unknown command\n");
            break;
    }
    
    actualizar_luces();
    
    char response[64];
	int len = snprintf(response, sizeof (response),
			"{\"jardin\" : %d,"
			" \"sotano\" : %d}", jardin,sotano);
	if (len < sizeof (response))
		websocket_write(pcb, (unsigned char *) response, len, WS_TEXT_MODE);
}

void websocket_open_cb(struct tcp_pcb *pcb, const char *uri)
{
    printf("WS URI: %s\n", uri);
    if (!strcmp(uri, "/stream")) {
        printf("request for streaming\n");
        xTaskCreate(&websocket_task, "websocket_task", 256, (void *) pcb, 2, NULL);
    }
}


void httpd_task(void *pvParameters)
{
	tCGI pCGIs[] = {
        {"/api", (tCGIHandler) api_cgi_handler},
    };
	
	const char *pcConfigSSITags[] = {
        "jaron",
        "jaroff",
        "soton",
        "sotoff",
    };
	
	http_set_cgi_handlers(pCGIs, sizeof (pCGIs) / sizeof (pCGIs[0]));
	http_set_ssi_handler((tSSIHandler) ssi_handler, pcConfigSSITags,
            sizeof (pcConfigSSITags) / sizeof (pcConfigSSITags[0]));
    websocket_register_callbacks((tWsOpenHandler) websocket_open_cb,
            (tWsHandler) websocket_cb);
    httpd_init();
    
    while(1);
}

#define LED_PIN 2

void actualizar_luces(){
	gpio_write(gpio_jardin, jardin);
    gpio_write(gpio_sotano, sotano);
}

void user_init(void)
{
	uart_set_baud(0, 115200);
    printf("SDK version:%s\n", sdk_system_get_sdk_version());

    struct sdk_station_config config = {
        .ssid = WIFI_SSID,
        .password = WIFI_PASS,
    };

    /* required to call wifi_set_opmode before station_set_config */
    sdk_wifi_set_opmode(STATION_MODE);
    sdk_wifi_station_set_config(&config);
    sdk_wifi_station_connect();

	jardin = 0;
	sotano = 0;
    /* turn off LED */
    gpio_enable(gpio_jardin, GPIO_OUTPUT);
    gpio_enable(gpio_sotano, GPIO_OUTPUT);
    gpio_write(gpio_jardin, 0);
    gpio_write(gpio_sotano, 0);

    /* initialize tasks */
    xTaskCreate(&httpd_task, "HTTP Daemon", 256, NULL, 2, NULL);
}
